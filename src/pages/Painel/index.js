import React from 'react';
import {View, Text, TouchableOpacity, ImageBackground} from 'react-native';
import Card from './components';
import Img from '../../image/background/01.jpeg';

const Painel = ({navigation}) => {
  return (
    <View>
      <View style={{alignItems: 'center', backgroundColor: '#c6c6c6'}}>
        <Text style={{fontSize: 32, marginTop: 10, color: '#333'}}>
          INDUSTRIA 4.0
        </Text>
      </View>

      <ImageBackground source={Img} style={{width: '100%', height: '100%'}}>
        <View>
          <View
            style={{
              marginTop: 30,
              flexDirection: 'row',
              justifyContent: 'space-around',
            }}>
            <Card
              color="#73d13d"
              text="COMANDOS"
              icon="menu"
              size={80}
              onPress={() => navigation.navigate('Comando')}
            />
            <Card color="#237804" text="USUARIO" icon="users" />
          </View>

          <View
            style={{
              marginTop: 30,
              flexDirection: 'row',
              justifyContent: 'space-around',
            }}>
            <Card
              color="#40a9ff"
              text="TEMPO"
              icon="back-in-time"
              size={70}
              onPress={() => navigation.navigate('Tempo')}
            />
            <Card
              color="#096dd9"
              text="AJUSTES"
              icon="cog"
              size={70}
              onPress={() => navigation.navigate('Ajuste')}
            />
          </View>
          <View
            style={{
              marginTop: 30,
              flexDirection: 'row',
              justifyContent: 'space-around',
            }}>
            <Card
              color="#fadb14"
              text="ALARMES"
              icon="bell"
              onPress={() => navigation.navigate('Alarme')}
            />
            <Card color="#f68700" text="MANUT" icon="tools" />
          </View>
        </View>
      </ImageBackground>
    </View>
  );
};

export default Painel;

Painel.navigationOptions = {
  headerShown: false,
};
