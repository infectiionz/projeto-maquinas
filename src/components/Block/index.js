import React from 'react';
import {StyleSheet, View, Animated} from 'react-native';
import {Colors} from '../Themes';

function Block(props) {
    function handleMargins() {
        const {margin} = props;
        if (typeof margin === 'number') {
            return {
                marginTop: margin,
                marginRight: margin,
                marginBottom: margin,
                marginLeft: margin,
            };
        }

        if (typeof margin === 'object') {
            const marginSize = Object.keys(margin).length;
            switch (marginSize) {
                case 1:
                    return {
                        marginTop: margin[0],
                        marginRight: margin[0],
                        marginBottom: margin[0],
                        marginLeft: margin[0],
                    };
                case 2:
                    return {
                        marginTop: margin[0],
                        marginRight: margin[1],
                        marginBottom: margin[0],
                        marginLeft: margin[1],
                    };
                case 3:
                    return {
                        marginTop: margin[0],
                        marginRight: margin[1],
                        marginBottom: margin[2],
                        marginLeft: margin[1],
                    };
                default:
                    return {
                        marginTop: margin[0],
                        marginRight: margin[1],
                        marginBottom: margin[2],
                        marginLeft: margin[3],
                    };
            }
        }
    }

    function handlePaddings() {
        const {padding} = props;
        if (typeof padding === 'number') {
            return {
                paddingTop: padding,
                paddingRight: padding,
                paddingBottom: padding,
                paddingLeft: padding,
            };
        }

        if (typeof padding === 'object') {
            const paddingSize = Object.keys(padding).length;
            switch (paddingSize) {
                case 1:
                    return {
                        paddingTop: padding[0],
                        paddingRight: padding[0],
                        paddingBottom: padding[0],
                        paddingLeft: padding[0],
                    };
                case 2:
                    return {
                        paddingTop: padding[0],
                        paddingRight: padding[1],
                        paddingBottom: padding[0],
                        paddingLeft: padding[1],
                    };
                case 3:
                    return {
                        paddingTop: padding[0],
                        paddingRight: padding[1],
                        paddingBottom: padding[2],
                        paddingLeft: padding[1],
                    };
                default:
                    return {
                        paddingTop: padding[0],
                        paddingRight: padding[1],
                        paddingBottom: padding[2],
                        paddingLeft: padding[3],
                    };
            }
        }
    }
    function handleRadius() {
        const {radius} = props;
        if (typeof radius === 'number') {
            return {
                borderTopLeftRadius: radius,
                borderTopRightRadius: radius,
                borderBottomRightRadius: radius,
                borderBottomLeftRadius: radius,
            };
        }

        if (typeof radius === 'object') {
            const paddingSize = Object.keys(radius).length;
            switch (paddingSize) {
                case 1:
                    return {
                        borderTopLeftRadius: radius[0],
                        borderTopRightRadius: radius[0],
                        borderBottomRightRadius: radius[0],
                        borderBottomLeftRadius: radius[0],
                    };
                case 2:
                    return {
                        borderTopLeftRadius: radius[0],
                        borderTopRightRadius: radius[1],
                        borderBottomRightRadius: radius[0],
                        borderBottomLeftRadius: radius[1],
                    };
                case 3:
                    return {
                        borderTopLeftRadius: radius[0],
                        borderTopRightRadius: radius[1],
                        borderBottomRightRadius: radius[2],
                        borderBottomLeftRadius: radius[1],
                    };
                default:
                    return {
                        borderTopLeftRadius: radius[0],
                        borderTopRightRadius: radius[1],
                        borderBottomRightRadius: radius[2],
                        borderBottomLeftRadius: radius[3],
                    };
            }
        }
    }

    function handleSize() {
        const {size} = props;
        if (typeof size === 'number') {
            return {
                width: size,
                height: size,
            };
        }

        if (typeof size === 'object') {
            const paddingSize = Object.keys(size).length;
            switch (paddingSize) {
                case 1:
                    return {
                        width: size[0],
                        height: size[0],
                    };
                case 2:
                    return {
                        width: size[0],
                        height: size[1],
                    };
                default:
                    return {
                        width: size[0],
                        height: size[1],
                    };
            }
        }
    }
    function handleBorder() {
        const {border} = props;
        return {
            borderWidth: border[0] ? border[0] : 1,
            borderColor: border[1] ? border[1] : Colors.gray2,
        };
    }

    const {
        width,
        height,
        flex,
        row,
        column,
        center,
        middle,
        left,
        right,
        top,
        bottom,
        absolute,

        border,
        radius,
        size,
        shadow,
        space,
        padding,
        margin,
        animated,
        wrap,
        style,

        between,
        around,
        opacity,
        primary,
        secondary,
        yellow,
        green,
        background,
        white,
        red,
        black,
        color,
        gray1,
        gray2,
        gray3,

        children,
        ...rest
    } = props;

    const styles = StyleSheet.create({
        block: {
            flex: 1,
        },
        row: {
            flexDirection: 'row',
        },
        column: {
            flexDirection: 'column',
        },

        center: {
            alignItems: 'center',
        },
        middle: {
            justifyContent: 'center',
        },
        left: {
            justifyContent: 'flex-start',
        },
        right: {
            justifyContent: 'flex-end',
        },
        top: {
            justifyContent: 'flex-start',
        },
        bottom: {
            justifyContent: 'flex-end',
        },
        absolute: {
            position: 'absolute',
        },
        shadow: {
            backgroundColor: '#fff',
            shadowColor: 'rgba(0, 0, 0, 0.1)',
            shadowOffset: {width: 0, height: 4},
            shadowOpacity: 0.1,
            shadowRadius: 13,
            elevation: 5,
        },

        opacity: {opacity},
        primary: {backgroundColor: Colors.primaryColor},
        secondary: {backgroundColor: Colors.secondaryColor},
        red: {backgroundColor: Colors.red},
        background: {backgroundColor: Colors.background},
        yellow: {backgroundColor: Colors.yellow},
        green: {backgroundColor: Colors.green},
        black: {backgroundColor: Colors.black},
        white: {backgroundColor: Colors.white},
        gray1: {backgroundColor: Colors.gray1},
        gray2: {backgroundColor: Colors.gray2},
        gray3: {backgroundColor: Colors.gray3},
        between: {justifyContent: 'space-between'},
        around: {justifyContent: 'space-around'},
    });

    const blockStyles = [
        styles.block,

        flex ? {flex} : {flex: 0},
        row && styles.row,
        column && styles.column,
        center && styles.center,
        middle && styles.middle,
        left && styles.left,
        absolute && styles.absolute,
        border && {...handleBorder()},

        red && styles.red,
        white && styles.white,
        background && styles.background,
        secondary && styles.secondary,
        yellow && styles.yellow,
        black && styles.black,
        green && styles.green,
        gray1 && styles.gray1,
        gray2 && styles.gray2,
        gray3 && styles.gray3,

        between && styles.between,
        around && styles.around,
        opacity && styles.opacity,
        primary && styles.primary,
        right && styles.right,
        top && styles.top,
        bottom && styles.bottom,
        margin && {...handleMargins()},
        padding && {...handlePaddings()},
        radius && {...handleRadius()},
        size && {...handleSize()},
        shadow && styles.shadow,
        space && {justifyContent: `space-${space}`},
        wrap && {flexWrap: 'wrap'},
        color && styles[color],
        color && !styles[color] && {backgroundColor: color},
        width && {width},
        height && {height},
        style && style,
    ];

    if (animated) {
        return (
            <Animated.View style={blockStyles} {...rest}>
                {children}
            </Animated.View>
        );
    }

    return (
        <View style={blockStyles} {...rest}>
            {children}
        </View>
    );
}

export default Block;