import firebase from 'firebase';

const firebaseConfig = {
  apiKey: 'AIzaSyBVdVPqe5Y8_MVM4K7m8xybWvAG5EyQab0',
  authDomain: 'projeto-maquinas.firebaseapp.com',
  databaseURL: 'https://projeto-maquinas.firebaseio.com',
  projectId: 'projeto-maquinas',
  storageBucket: 'projeto-maquinas.appspot.com',
  messagingSenderId: '463909835811',
  appId: '1:463909835811:web:2ab6db69f8df0a53fd0d96',
  measurementId: 'G-4JNGQXVEV7',
};

export default firebase.initializeApp(firebaseConfig);
