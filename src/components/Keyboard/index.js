import React from 'react';
import {
  I18nManager,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

const {isRTL} = I18nManager;
const BACKSPACE_ICON = Platform.OS === 'ios' ? 'ios-backspace' : 'md-backspace';

const styles = StyleSheet.create({
  container: {
    flexDirection: isRTL ? 'row-reverse' : 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    paddingVertical: 8,
    backgroundColor: '#d9d9d9',
  },
  keyboardButton: {
    width: '32%',
    height: 64,
    justifyContent: 'center',
    alignItems: 'center',
  },
  number: {
    fontWeight: '300',
    fontSize: 24,
    color: '#333',
  },
  actionButtonTitle: {
    padding: 12,
    fontWeight: '500',
    fontSize: 50,
    color: '#333',
  },
});

const NumericKeyboard = ({actionButtonTitle, value, setValue}) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        onPress={() => setValue(`${value}1`)}
        style={styles.keyboardButton}>
        <Text style={styles.number}>1</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => setValue(`${value}2`)}
        style={styles.keyboardButton}>
        <Text style={styles.number}>2</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => setValue(`${value}3`)}
        style={styles.keyboardButton}>
        <Text style={styles.number}>3</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => setValue(`${value}4`)}
        style={styles.keyboardButton}>
        <Text style={styles.number}>4</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => setValue(`${value}5`)}
        style={styles.keyboardButton}>
        <Text style={styles.number}>5</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => setValue(`${value}6`)}
        style={styles.keyboardButton}>
        <Text style={styles.number}>6</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => setValue(`${value}7`)}
        style={styles.keyboardButton}>
        <Text style={styles.number}>7</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => setValue(`${value}8`)}
        style={styles.keyboardButton}>
        <Text style={styles.number}>8</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => setValue(`${value}9`)}
        style={styles.keyboardButton}>
        <Text style={styles.number}>9</Text>
      </TouchableOpacity>
      {actionButtonTitle ? (
        <TouchableOpacity
          onPress={() => setValue(actionButtonTitle)}
          style={[styles.keyboardButton, {marginTop: -15}]}>
          <Text style={[styles.actionButtonTitle]}>
            {actionButtonTitle.toUpperCase()}
          </Text>
        </TouchableOpacity>
      ) : (
        <View style={styles.keyboardButton} />
      )}
      <TouchableOpacity
        onPress={() => setValue(`${value}0`)}
        style={styles.keyboardButton}>
        <Text style={styles.number}>0</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => setValue('')}
        style={styles.keyboardButton}>
        <Icon name={BACKSPACE_ICON} style={styles.number} />
      </TouchableOpacity>
    </View>
  );
};

export default NumericKeyboard;
