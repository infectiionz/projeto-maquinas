import React from 'react';
import {StatusBar, Text} from 'react-native';
import Routes from './routes';

export default () => {
  return (
    <>
      <StatusBar backgroundColor="#c6c6c6" barStyle="dark-content" />
      <Routes />
    </>
  );
};
