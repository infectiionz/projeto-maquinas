import React from 'react';
import Icon from 'react-native-vector-icons/Entypo';
import {View, Text, TouchableOpacity, Dimensions} from 'react-native';

const Card = ({color, text, icon, onPress, size}) => {
  const {height, width} = Dimensions.get('window');
  const SizeCard = height / 3 - 68;

  return (
    <TouchableOpacity onPress={onPress}>
      <View
        style={{
          justifyContent: 'space-evenly',
          alignItems: 'center',
          borderRadius: 15,
          width: SizeCard,
          height: SizeCard,
          backgroundColor: color,
        }}>
        <Icon name={icon} color="#fff" size={size || 70} />
        <Text style={{fontSize: 20, color: '#fff'}}>{text}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default Card;
