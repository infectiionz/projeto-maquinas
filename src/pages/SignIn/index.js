import React, {useRef, useState} from 'react';
import {
  View,
  Dimensions,
  Text,
  TextInput,
  TouchableOpacity,
  ImageBackground,
  Alert,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import firebase from '../../services';
import Img from '../../image/backgroundFabrica/01.jpg';

const SignIn = ({navigation}) => {
  const [email, setEmail] = useState('herickson.pereira@hotmail.com');
  const [password, setPassword] = useState('haop5009');
  let passwordRef = useRef(null);

  const [secure, setSecure] = useState(true);
  const auth = firebase.auth();

  async function CreateUser() {
    try {
      // const usuario = await auth.createUserWithEmailAndPassword(   email,   password );
      const usuario = await auth.signInWithEmailAndPassword(email, password);
      navigation.navigate('Painel');
    } catch (err) {
      Alert.alert('info', 'Usuario não existe');
    }
  }

  const {width} = Dimensions.get('window');
  return (
    <ImageBackground source={Img} style={{width: '100%', height: '100%'}}>
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <View
          style={{
            borderRadius: 10,
            maxWidth: 400,
            height: 200,
            width: width - 50,
            elevation: 3,
            backgroundColor: 'rgba(255,255,255,0.8)',
          }}>
          <View
            style={{
              marginHorizontal: 20,
              borderWidth: 1,
              borderColor: '#a9a9a9',
              borderRadius: 10,
              marginVertical: 20,
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <View style={{marginHorizontal: 10}}>
              <Icon name="md-mail" size={27} color="#33f" />
            </View>
            <TextInput
              keyboardType="email-address"
              autoCapitalize="none"
              returnKeyType="next"
              placeholder="E-mail"
              style={{fontSize: 22, flex: 1}}
              value={email}
              onChangeText={setEmail}
              onSubmitEditing={() => passwordRef.current.focus()}
            />
          </View>
          <View
            style={{
              marginHorizontal: 20,
              borderWidth: 1,
              borderColor: '#a9a9a9',
              borderRadius: 10,
              marginVertical: 20,
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <View style={{marginHorizontal: 10}}>
              <Icon name="md-lock" size={30} color="#33f" />
            </View>
            <TextInput
              ref={passwordRef}
              placeholder="Senha"
              secureTextEntry={secure}
              style={{fontSize: 22, flex: 1}}
              value={password}
              onChangeText={setPassword}
            />
            <TouchableOpacity onPress={() => setSecure(!secure)}>
              <View style={{marginHorizontal: 10}}>
                <Icon
                  name={secure ? 'md-eye' : 'md-eye-off'}
                  size={27}
                  color="#000"
                />
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <TouchableOpacity onPress={() => CreateUser()}>
          <View
            style={{
              marginTop: 20,
              justifyContent: 'center',
              alignItems: 'center',
              width: width - 50,
              backgroundColor: '#11f',
              height: 45,
              borderRadius: 10,
            }}>
            <Text style={{fontSize: 20, fontWeight: 'bold', color: '#fff'}}>
              Login
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    </ImageBackground>
  );
};

export default SignIn;
SignIn.navigationOptions = {
  headerShown: false,
};
