import React, {useState} from 'react';
import {Block, Text, Colors} from '../../components';
import Card from './Card';
import {ScrollView} from 'react-native';
// import { Container } from './styles';

const Ajuste = () => {
  const [input1, setInput1] = useState('');
  const [input2, setInput2] = useState('');
  const [input3, setInput3] = useState('');
  const [input4, setInput4] = useState('');
  const [input5, setInput5] = useState('');
  const [input6, setInput6] = useState('');
  const [input7, setInput7] = useState('');
  const [input8, setInput8] = useState('');
  const [input9, setInput9] = useState('');
  const [input10, setInput10] = useState('');
  const [input11, setInput11] = useState('');

  return (
    <Block color={Colors.background2} flex={1}>
      <Block color="#c0c0c0" margin={10} flex={1} radius={10}>
        <Block margin={10}>
          <ScrollView>
            <Block row>
              <Block flex={1} />
              <Block size={[70, 30]} margin={[0, 5]} center middle>
                <Text h2 bold>
                  Inicio
                </Text>
              </Block>
              <Block size={[70, 30]} margin={[0, 5]} center middle>
                <Text h2 bold>
                  Fim
                </Text>
              </Block>
            </Block>
            <Card label="Ajuste 1" value={input1} onChange={setInput1} />
            <Card label="Ajuste 2" value={input2} onChange={setInput2} />
            <Card label="Ajuste 3" value={input3} onChange={setInput3} />
            <Card label="Ajuste 4" value={input4} onChange={setInput4} />
            <Card label="Ajuste 5" value={input5} onChange={setInput5} />
            <Card label="Ajuste 6" value={input6} onChange={setInput6} />
            <Card label="Ajuste 7" value={input7} onChange={setInput7} />
            <Card label="Ajuste 8" value={input8} onChange={setInput8} />
            <Card label="Ajuste 9" value={input9} onChange={setInput9} />
            <Card label="Ajuste 10" value={input10} onChange={setInput10} />
            <Card label="Ajuste 11" value={input11} onChange={setInput11} />
          </ScrollView>
        </Block>
      </Block>
    </Block>
  );
};

export default Ajuste;
