import React, {useState} from 'react';
import {Text, View, ScrollView} from 'react-native';
import firebase from '../../services';
 import Keyboard from '../../components/Keyboard';

const Alarme = () => {
  const [AvisoMaqAnteriorParada, setAvisoMaqAnteriorParada] = useState('');
  const [AvisoMaqFrenteParada, setAvisoMaqFrenteParada] = useState('');
  const [AvisoMinimoCaixas, setAvisoMinimoCaixas] = useState('');
  const [FalhaAcumuloSaida, setFalhaAcumuloSaida] = useState('');

  const [AvisoMaquina, setAvisoMaquina] = useState('');
  const [FalhaMaquina, setFalhaMaquina] = useState('');

  const [FalhaEmergenciaAcionada, setFalhaEmergenciaAcionada] = useState('');
  const [FalhaMinimoFrasco, setFalhaMinimoFrasco] = useState('');
  const [FalhaPortaAberta, setFalhaPortaAberta] = useState('');

  const db = firebase.database();
  db.ref('/maquina1').on('value', (snapshot) => {
    if (AvisoMaqAnteriorParada != snapshot.val().AvisoMaqAnteriorParada) {
      setAvisoMaqAnteriorParada(snapshot.val().AvisoMaqAnteriorParada);
    }
    if (AvisoMaqFrenteParada != snapshot.val().AvisoMaqFrenteParada) {
      setAvisoMaqFrenteParada(snapshot.val().AvisoMaqFrenteParada);
    }
    if (AvisoMinimoCaixas != snapshot.val().AvisoMinimoCaixas) {
      setAvisoMinimoCaixas(snapshot.val().AvisoMinimoCaixas);
    }
    if (FalhaAcumuloSaida != snapshot.val().FalhaAcumuloSaida) {
      setFalhaAcumuloSaida(snapshot.val().FalhaAcumuloSaida);
    }
    if (FalhaEmergenciaAcionada != snapshot.val().FalhaEmergenciaAcionada) {
      setFalhaEmergenciaAcionada(snapshot.val().FalhaEmergenciaAcionada);
    }
    if (FalhaMinimoFrasco != snapshot.val().FalhaMinimoFrasco) {
      setFalhaMinimoFrasco(snapshot.val().FalhaMinimoFrasco);
    }
    if (FalhaPortaAberta != snapshot.val().FalhaPortaAberta) {
      setFalhaPortaAberta(snapshot.val().FalhaPortaAberta);
    }
    if (AvisoMaquina != snapshot.val().aviso) {
      setAvisoMaquina(snapshot.val().aviso);
    }
    if (FalhaMaquina != snapshot.val().falha) {
      setFalhaMaquina(snapshot.val().falha);
    }
  });
  return (
    <>
      <ScrollView>
        {(!!AvisoMaquina ? true : !!FalhaMaquina) ? (
          <View
            style={{backgroundColor: '#c0c0c0', margin: 10, borderWidth: 3}}>
            <View style={{margin: 10}}>
              <View style={{alignItems: 'center'}}>
                <Text style={{fontWeight: 'bold', fontSize: 22}}>Alarmes</Text>
              </View>
              {AvisoMaqAnteriorParada ? (
                <Text style={{fontWeight: 'bold', fontSize: 18, color: '#ff7'}}>
                  Maquina Anterior Parada
                </Text>
              ) : null}
              {AvisoMaqFrenteParada ? (
                <Text style={{fontWeight: 'bold', fontSize: 18, color: '#ff7'}}>
                  Maquina da frente parada
                </Text>
              ) : null}
              {AvisoMinimoCaixas ? (
                <Text style={{fontWeight: 'bold', fontSize: 18, color: '#ff7'}}>
                  Minimo de caixa
                </Text>
              ) : null}
              {FalhaAcumuloSaida ? (
                <Text style={{fontWeight: 'bold', fontSize: 18, color: '#f44'}}>
                  Acumulo na Saida
                </Text>
              ) : null}
              {FalhaEmergenciaAcionada ? (
                <Text style={{fontWeight: 'bold', fontSize: 18, color: '#f44'}}>
                  Emergencia Acionada
                </Text>
              ) : null}
              {FalhaMinimoFrasco ? (
                <Text style={{fontWeight: 'bold', fontSize: 18, color: '#f44'}}>
                  Minimo frasco
                </Text>
              ) : null}
              {FalhaPortaAberta ? (
                <Text style={{fontWeight: 'bold', fontSize: 18, color: '#f44'}}>
                  Porta Aberta
                </Text>
              ) : null}
            </View>
          </View>
        ) : null}
        <Keyboard actionButtonTitle='.' />
      </ScrollView>
    </>
  );
};

export default Alarme;
