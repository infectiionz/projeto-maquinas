import React, {useState, useRef, useEffect} from 'react';
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
  ScrollView,
  Animated,
} from 'react-native';
import firebase from '../../services';
import CardInput from '../../components/cardInput';
import messaging from '@react-native-firebase/messaging';
function SRC() {
  let leitura = 0;
  const {height} = Dimensions.get('window');
  const [StatusMaquina, setStatusMaquina] = useState('');
  const [AvisoMaquina, setAvisoMaquina] = useState('');
  const [FalhaMaquina, setFalhaMaquina] = useState('');
  const [comm, setComm] = useState('');

  const [nivel, setNivel] = useState(0);
  const [vel, setVel] = useState(0);
  const [temp, setTemp] = useState(0);
  const [PressaoBar, setPressaoBar] = useState('');
  const [Encoder, setEncoder] = useState(0);

  const [AvisoMaqAnteriorParada, setAvisoMaqAnteriorParada] = useState('');
  const [AvisoMaqFrenteParada, setAvisoMaqFrenteParada] = useState('');
  const [AvisoMinimoCaixas, setAvisoMinimoCaixas] = useState('');
  const [FalhaAcumuloSaida, setFalhaAcumuloSaida] = useState('');

  const [FalhaEmergenciaAcionada, setFalhaEmergenciaAcionada] = useState('');
  const [FalhaMinimoFrasco, setFalhaMinimoFrasco] = useState('');
  const [FalhaPortaAberta, setFalhaPortaAberta] = useState('');

  const [AnimatedAviso, setAnimatedAviso] = useState(new Animated.Value(0));
  const [AnimatedFalha, setAnimatedFalha] = useState(new Animated.Value(0));

  const [input, setInput] = useState('');

  const db = firebase.database();

  db.ref('/maquina1').on('value', (snapshot) => {
    if (nivel != snapshot.val().nivel) {
      setNivel(snapshot.val().nivel);
    }
    if (StatusMaquina != snapshot.val().status) {
      setStatusMaquina(snapshot.val().status);
      console.log(snapshot.val().status);
    }
    if (AvisoMaquina != snapshot.val().aviso) {
      setAvisoMaquina(snapshot.val().aviso);
    }
    if (FalhaMaquina != snapshot.val().falha) {
      setFalhaMaquina(snapshot.val().falha);
    }
    if (comm != snapshot.val().comm) {
      setComm(snapshot.val().comm);
    }
    if (vel != snapshot.val().rpm) {
      setVel(snapshot.val().rpm);
      console.log(snapshot.val().rpm);
    }
    if (PressaoBar != snapshot.val().pressao) {
      setPressaoBar(snapshot.val().pressao);
      console.log(snapshot.val().pressao);
    }
    if (temp != snapshot.val().temp) {
      setTemp(snapshot.val().temp);
      console.log(snapshot.val().temp);
    }
    if (Encoder != snapshot.val().Encoder) {
      setEncoder(snapshot.val().Encoder);
      console.log(snapshot.val().Encoder);
    }

    if (AvisoMaqAnteriorParada != snapshot.val().AvisoMaqAnteriorParada) {
      setAvisoMaqAnteriorParada(snapshot.val().AvisoMaqAnteriorParada);
    }
    if (AvisoMaqFrenteParada != snapshot.val().AvisoMaqFrenteParada) {
      setAvisoMaqFrenteParada(snapshot.val().AvisoMaqFrenteParada);
    }
    if (AvisoMinimoCaixas != snapshot.val().AvisoMinimoCaixas) {
      setAvisoMinimoCaixas(snapshot.val().AvisoMinimoCaixas);
    }
    if (FalhaAcumuloSaida != snapshot.val().FalhaAcumuloSaida) {
      setFalhaAcumuloSaida(snapshot.val().FalhaAcumuloSaida);
    }
    if (FalhaEmergenciaAcionada != snapshot.val().FalhaEmergenciaAcionada) {
      setFalhaEmergenciaAcionada(snapshot.val().FalhaEmergenciaAcionada);
    }
    if (FalhaMinimoFrasco != snapshot.val().FalhaMinimoFrasco) {
      setFalhaMinimoFrasco(snapshot.val().FalhaMinimoFrasco);
    }
    if (FalhaPortaAberta != snapshot.val().FalhaPortaAberta) {
      setFalhaPortaAberta(snapshot.val().FalhaPortaAberta);
    }
  });

  useEffect(() => {
    if (AvisoMaquina) {
      Animated.loop(
        Animated.sequence([
          Animated.timing(AnimatedAviso, {
            toValue: 1,
            duration: 300,
            useNativeDriver: false,
          }),
          Animated.delay(500),
          Animated.timing(AnimatedAviso, {
            toValue: 0,
            duration: 300,
            useNativeDriver: false,
          }),
          Animated.delay(500),
          Animated.timing(AnimatedAviso, {
            toValue: 1,
            duration: 300,
            useNativeDriver: false,
          }),
          Animated.delay(500),
          Animated.timing(AnimatedAviso, {
            toValue: 0,
            duration: 300,
            useNativeDriver: false,
          }),
          Animated.delay(500),
          Animated.timing(AnimatedAviso, {
            toValue: 1,
            duration: 300,
            useNativeDriver: false,
          }),
          Animated.delay(500),
        ]),
      ).start();
    }
  }, [AvisoMaquina]);

  useEffect(() => {
    async function Messaging() {
      const enabled = await messaging().hasPermission();
      if (enabled) {
        const token = await messaging().getToken();
        console.log('Token: ', token);
        await messaging().getInitialNotification();
      } else {
        await messaging().requestPermission();
      }
    }
    Messaging();
  }, []);

  return (
    <View style={{flex: 1}}>
      <ImageBackground
        source={require('./image.jpg')}
        style={{width: '100%', height: '100%'}}>
        <ScrollView>
          <>
            <View style={{flex: 1}}>
              <View
                style={{
                  borderWidth: 3,
                  margin: 10,
                  backgroundColor: '#969696',
                  flexDirection: 'row-reverse',
                }}>
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'space-around',
                    alignItems: 'center',
                  }}>
                  <TouchableOpacity
                    onPress={() => {
                      db.ref('maquina1').update({PedidoStatus: 'Ligar'});
                    }}>
                    <View
                      style={{
                        borderWidth: 3,
                        backgroundColor: '#636571',
                        margin: 5,
                        width: 100,
                        height: 50,
                        borderRadius: 5,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      <Text style={{color: '#fff'}}>LIGA</Text>
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity
                    onPress={() => {
                      db.ref('maquina1').update({PedidoStatus: 'Desligar'});
                    }}>
                    <View
                      style={{
                        borderWidth: 3,
                        backgroundColor: '#636571',
                        margin: 5,
                        width: 100,
                        height: 50,
                        borderRadius: 5,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      <Text style={{color: '#fff'}}>DESLIGA</Text>
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity
                    onPress={() => {
                      db.ref('maquina1').update({PedidoReset: true});
                    }}>
                    <View
                      style={{
                        borderWidth: 3,
                        backgroundColor: '#636571',
                        margin: 5,
                        width: 100,
                        height: 50,
                        borderRadius: 5,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      <Text style={{color: '#fff'}}>RESET</Text>
                    </View>
                  </TouchableOpacity>
                </View>
                <View
                  style={{
                    flex: 1,
                    backgroundColor: '#c0c0c0',
                    flexDirection: 'row',
                  }}>
                  <View>
                    <View style={{width: 15, height: 3}} />
                  </View>
                  <View>
                    <View
                      style={{
                        flexDirection: 'row',
                        backgroundColor: '#c0c0c0',
                        marginTop: 10,
                        justifyContent: 'space-between',
                        alignItems: 'center',
                      }}>
                      <Text style={{fontSize: 22, fontWeight: 'bold'}}>
                        STATUS
                      </Text>
                      <View
                        style={{
                          marginLeft: 10,
                          width: 35,
                          height: 35,
                          backgroundColor: StatusMaquina ? '#9f6' : '#f30',
                          justifyContent: 'space-between',
                          alignItems: 'center',
                        }}
                      />
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        backgroundColor: '#c0c0c0',
                        marginTop: 10,
                        justifyContent: 'space-between',
                        alignItems: 'center',
                      }}>
                      <Text style={{fontSize: 22, fontWeight: 'bold'}}>
                        AVISO
                      </Text>
                      <View
                        style={{
                          backgroundColor: AvisoMaquina ? '#ffff00' : '#464646',
                          width: 35,
                          height: 35,
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}>
                        <Animated.View
                          style={{
                            opacity: AnimatedAviso,

                            width: 35,
                            height: 35,
                            backgroundColor: '#464646',
                          }}
                        />
                      </View>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        backgroundColor: '#c0c0c0',
                        marginTop: 10,
                        justifyContent: 'space-between',
                        alignItems: 'center',
                      }}>
                      <Text style={{fontSize: 22, fontWeight: 'bold'}}>
                        EM FALHA
                      </Text>
                      <View
                        style={{
                          marginLeft: 10,
                          width: 35,
                          height: 35,
                          backgroundColor: FalhaMaquina ? '#f30' : '#464646',
                        }}>
                        <Animated.View
                          style={{
                            opacity: AnimatedAviso,

                            width: 35,
                            height: 35,
                            backgroundColor: '#464646',
                          }}
                        />
                      </View>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        backgroundColor: '#c0c0c0',
                        marginTop: 10,
                        justifyContent: 'space-between',
                        alignItems: 'center',
                      }}>
                      <Text style={{fontSize: 22, fontWeight: 'bold'}}>
                        COMM
                      </Text>
                      <View
                        style={{
                          marginLeft: 10,
                          width: 35,
                          height: 35,
                          backgroundColor: comm ? '#9f6' : '#464646',
                        }}
                      />
                    </View>
                    <View style={{width: 15, height: 15}} />
                  </View>
                  <View>
                    <View style={{width: 15, height: 10}} />
                  </View>
                </View>
              </View>

              <View style={{borderWidth: 3, margin: 10}}>
                <View
                  style={{flexDirection: 'row', backgroundColor: '#c0c0c0'}}>
                  <View style={{flex: 4}} />
                  <View style={{flex: 2, alignItems: 'center'}}>
                    <Text style={{fontSize: 18}}>Atual</Text>
                  </View>
                  <View style={{flex: 2, alignItems: 'center'}}>
                    <Text style={{fontSize: 18}}>Set</Text>
                  </View>
                  <View style={{flex: 2}} />
                </View>
                <CardInput
                  valorAtual={vel}
                  text="Velocidade"
                  url="PedidoVel"
                  atual
                  set
                />
                <CardInput
                  valorAtual={PressaoBar}
                  text="Pressao Bar"
                  url="PedidoBar"
                  atual
                  set
                />
                <CardInput
                  valorAtual={temp}
                  text="Temperatura"
                  url="PedidoTemp"
                  atual
                  set
                />
                <CardInput
                  valorAtual={Encoder}
                  text="Encoder"
                  url="teste"
                  atual
                />
              </View>
            </View>

            {(!!AvisoMaquina ? true : !!FalhaMaquina) ? (
              <View
                style={{
                  backgroundColor: '#c0c0c0',
                  margin: 10,
                  borderWidth: 3,
                }}>
                <View style={{margin: 10}}>
                  <View style={{alignItems: 'center'}}>
                    <Text style={{fontWeight: 'bold', fontSize: 22}}>
                      Alarmes
                    </Text>
                  </View>
                  {AvisoMaqAnteriorParada ? (
                    <Text
                      style={{fontWeight: 'bold', fontSize: 18, color: '#ff7'}}>
                      Maquina Anterior Parada
                    </Text>
                  ) : null}
                  {AvisoMaqFrenteParada ? (
                    <Text
                      style={{fontWeight: 'bold', fontSize: 18, color: '#ff7'}}>
                      Maquina da frente parada
                    </Text>
                  ) : null}
                  {AvisoMinimoCaixas ? (
                    <Text
                      style={{fontWeight: 'bold', fontSize: 18, color: '#ff7'}}>
                      Minimo de caixa
                    </Text>
                  ) : null}
                  {FalhaAcumuloSaida ? (
                    <Text
                      style={{fontWeight: 'bold', fontSize: 18, color: '#f44'}}>
                      Acumulo na Saida
                    </Text>
                  ) : null}
                  {FalhaEmergenciaAcionada ? (
                    <Text
                      style={{fontWeight: 'bold', fontSize: 18, color: '#f44'}}>
                      Emergencia Acionada
                    </Text>
                  ) : null}
                  {FalhaMinimoFrasco ? (
                    <Text
                      style={{fontWeight: 'bold', fontSize: 18, color: '#f44'}}>
                      Minimo frasco
                    </Text>
                  ) : null}
                  {FalhaPortaAberta ? (
                    <Text
                      style={{fontWeight: 'bold', fontSize: 18, color: '#f44'}}>
                      Porta Aberta
                    </Text>
                  ) : null}
                </View>
              </View>
            ) : null}
          </>
        </ScrollView>
      </ImageBackground>
    </View>
  );
}
export default SRC;
