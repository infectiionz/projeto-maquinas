export const Colors = {
  primaryColor: '#69c0ff',
  secondaryColor: '#096dd9',
  tertiaryColor: '#95de64',

  red: '#F3534A',
  yellow: '#FFE358',
  green: '#2CED84',
  // status bar color
  statusBarColor: '#fff',

  // gradient colors
  primaryGradientColor: '#5CAFEB',
  secondaryGradientColor: '#35C2DB',

  // Badges
  medicine: '#F3534A6B',
  conduct: '#5CAFEB80',
  exam: '#FFE35880',

  // text color
  text: '#323643',
  textBlack: '#323643',
  textInput: '#9DA3B4',

  background: '#FAFAFA',
  background2: '#E0E0E0',

  borderColor: '#9DA3B4',
  error: '#F3534A',
  black: '#323643',
  white: '#fff',

  gray1: '#454A5C',
  gray2: '#9DA3B4',
  gray3: '#C5CCD6',
  inputOutline: '#E0E0E0',
};

export const sizes = {
  // global sizes
  radius: 10,
  padding: 25,
  borderWidth: 1,
  // font sizes
  h1: 28,
  h2: 18,
  h3: 16,
  h4: 14,
  h5: 12,
  h6: 8,

  title: 18,
  header: 16,
  body: 14,
  caption: 12,
  avatar: 100,
  backHeader: 28,
};

export const fonts = {
  h1: {
    fontSize: sizes.h1,
  },
  h2: {
    fontSize: sizes.h2,
  },
  h3: {
    fontSize: sizes.h3,
  },
  h4: {
    fontSize: sizes.h4,
  },
  h5: {
    fontSize: sizes.h5,
  },
  h6: {
    fontSize: sizes.h6,
  },
  header: {
    fontSize: sizes.header,
  },
  title: {
    fontSize: sizes.title,
  },
  body: {
    fontSize: sizes.body,
  },
  caption: {
    fontSize: sizes.caption,
  },
};
