import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import Comando from './pages/Comando';
import Painel from './pages/Painel';
import Alarme from './pages/Alarme';
import SignIn from './pages/SignIn';
import Tempo from './pages/Tempo';

// como não tenho o arquivo index na pasta ajuste preciso passar o nome do arquivo poderia
// ser App.js ou App
import Ajuste from './pages/Ajuste/App';

export default createAppContainer(
  createStackNavigator({
    SignIn,
    Painel,
    Tempo,
    Comando,
    Alarme,
    Ajuste,
  }),
);
