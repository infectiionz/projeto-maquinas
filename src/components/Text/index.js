import React from 'react';
import {Text as Title, StyleSheet} from 'react-native';
import {Colors, sizes} from '../Themes';

function Text(props) {
    function handleMargins() {
        const {margin} = props;
        if (typeof margin === 'number') {
            return {
                marginTop: margin,
                marginRight: margin,
                marginBottom: margin,
                marginLeft: margin,
            };
        }

        if (typeof margin === 'object') {
            const marginSize = Object.keys(margin).length;
            switch (marginSize) {
                case 1:
                    return {
                        marginTop: margin[0],
                        marginRight: margin[0],
                        marginBottom: margin[0],
                        marginLeft: margin[0],
                    };
                case 2:
                    return {
                        marginTop: margin[0],
                        marginRight: margin[1],
                        marginBottom: margin[0],
                        marginLeft: margin[1],
                    };
                case 3:
                    return {
                        marginTop: margin[0],
                        marginRight: margin[1],
                        marginBottom: margin[2],
                        marginLeft: margin[1],
                    };
                default:
                    return {
                        marginTop: margin[0],
                        marginRight: margin[1],
                        marginBottom: margin[2],
                        marginLeft: margin[3],
                    };
            }
        }
    }

    const {
        h1,
        h2,
        h3,
        h4,
        h5,
        h6,
        title,
        body,
        caption,
        small,
        size,
        transform,
        align,
        // estilo da font
        regular,
        bold,
        semibold,
        medium,
        weight,
        light,
        center,
        right,
        spacing,
        height,
        margin,
        // cor
        color,
        accent,
        primary,
        secondary,
        tertiary,
        black,
        white,
        gray1,
        gray2,
        gray3,
        red,
        yellow,
        green,
        style,
        children,
        ...rest
    } = props;

    const styles = StyleSheet.create({
        text: {
            fontSize: 14,
            color: '#323643',
        },
        regular: {
            fontWeight: 'normal',
        },
        bold: {
            fontWeight: 'bold',
        },
        semibold: {
            fontWeight: '500',
        },
        medium: {
            fontWeight: '500',
        },
        light: {
            fontWeight: '200',
        },

        center: {textAlign: 'center'},
        right: {textAlign: 'right'},

        primary: {color: Colors.primaryColor},
        secondary: {color: Colors.secondaryColor},
        tertiary: {color: Colors.tertiaryColor},
        black: {color: Colors.black},
        white: {color: Colors.white},
        gray1: {color: Colors.gray1},
        gray2: {color: Colors.gray2},
        gray3: {color: Colors.gray3},
        red: {color: Colors.red},
        yellow: {color: Colors.yellow},
        green: {color: Colors.green},

        h1: {fontSize: sizes.h1},
        h2: {fontSize: sizes.h2},
        h3: {fontSize: sizes.h3},
        h4: {fontSize: sizes.h4},
        h5: {fontSize: sizes.h5},
        h6: {fontSize: sizes.h6},
    });

    const textStyles = [
        {color: '#323643'},
        styles.text,
        h1 && styles.h1,
        h2 && styles.h2,
        h3 && styles.h3,
        h4 && styles.h4,
        h5 && styles.h5,
        h6 && styles.h6,
        margin && {...handleMargins()},
        title && styles.title,
        body && styles.body,
        caption && styles.caption,
        small && styles.small,
        size && {fontSize: size},
        transform && {textTransform: transform},
        align && {textAlign: align},
        height && {lineHeight: height},
        spacing && {letterSpacing: spacing},
        weight && {fontWeight: weight},
        regular && styles.regular,
        bold && styles.bold,
        semibold && styles.semibold,
        medium && styles.medium,
        light && styles.light,
        center && styles.center,
        right && styles.right,
        color && styles[color],
        color && !styles[color] && {color},
        // cor
        accent && styles.accent,
        primary && styles.primary,
        secondary && styles.secondary,
        tertiary && styles.tertiary,
        black && styles.black,
        white && styles.white,
        gray1 && styles.gray1,
        gray2 && styles.gray2,
        gray3 && styles.gray3,
        red && styles.red,
        yellow && styles.yellow,
        green && styles.green,
        style,
    ];

    return (
        <Title style={textStyles} {...rest}>
            {children}
        </Title>
    );
}
export default Text;
