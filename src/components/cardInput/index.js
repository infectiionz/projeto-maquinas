import React, {useState} from 'react';
import {View, TextInput, Text, TouchableOpacity, Alert} from 'react-native';
import firebase from '../../services';

function CardInput({text, url, atual, set, valorAtual}) {
  const [input, setInput] = useState('');
  const db = firebase.database();

  async function Send() {
    if (input.length > 0) {
      db.ref('maquina1').update({[url]: input});
      setInput('');
    }
  }
  return (
    <View style={{flexDirection: 'row'}}>
      <View
        style={{
          flex: 4,
          backgroundColor: '#d9d9d9',
          height: 60,
          justifyContent: 'center',
        }}>
        <Text style={{fontSize: 18, marginLeft: 10}}>{text}</Text>
      </View>
      <View
        style={{
          flex: 2,
          backgroundColor: '#d9d9d9',
          height: 60,
        }}>
        <View
          style={{
            height: 40,
            margin: 7,
            borderWidth: 4,
            borderRadius: 4,
            backgroundColor: '#f8f8f8',
            borderColor: '#565656',
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Text style={{fontSize: 20}}>{valorAtual}</Text>
        </View>
      </View>
      <View
        style={{
          flex: 2,
          backgroundColor: '#d9d9d9',
          height: 60,
        }}>
        <View
          style={{
            margin: 7,
            height: 40,
            borderWidth: set && 4,
            borderRadius: set && 4,
            backgroundColor: '#f8f8f8',
            borderColor: '#565656',
          }}>
          {set && (
            <TextInput
              style={{fontSize: 22, height: 37, padding: 0}}
              keyboardType="numeric"
              value={input}
              onChangeText={setInput}
            />
          )}
        </View>
      </View>

      <View
        style={{
          flex: 2,
          backgroundColor: '#d9d9d9',
          height: 60,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        {set && (
          <TouchableOpacity onPress={Send}>
            <View
              style={{
                backgroundColor: set && '#696969',
                height: 45,
                width: 60,
                alignItems: 'center',
                justifyContent: 'center',
                borderRadius: 5,
                borderWidth: set && 3,
                borderColor: set && '#464646',
              }}>
              <Text style={{fontSize: 16, fontWeight: 'bold', color: '#fff'}}>
                Enviar
              </Text>
            </View>
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
}

export default CardInput;
