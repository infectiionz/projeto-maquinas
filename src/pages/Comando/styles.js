const styles = {
  title: {
    fontSize: 25,
  },
  text: {
    fontSize: 18,
  },
  content: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  input: {
    backgroundColor: '#d8d8d8',
  },
};

export default styles;
