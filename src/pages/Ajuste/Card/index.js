import React from 'react';
import {TextInput} from 'react-native';
import {Block, Text, Colors} from '../../../components';

export default ({label, current, onChange, value}) => {
  return (
    <Block margin={[5, 0]}>
      <Block row>
        <Block flex={1} />
        <Block size={[70, 30]} center middle margin={[0, 5]}>
          <Text h2 bold>
            Atual I
          </Text>
        </Block>
        <Block size={[70, 30]} margin={[0, 5]} center middle>
          <Text h2 bold>
            Atual F
          </Text>
        </Block>
      </Block>
      <Block row margin={[5, 0, 0, 0]}>
        <Block flex={1} middle>
          <Text h2>{label}</Text>
        </Block>
        <Block
          margin={[0, 5]}
          size={[70, 50]}
          border={[2.5, Colors.gray1]}
          center
          middle
          white
          radius={2}>
          <TextInput
            keyboardType="decimal-pad"
            onChangeText={onChange}
            value={value}
          />
        </Block>
        <Block
          margin={[0, 5]}
          size={[70, 50]}
          border={[2.5, Colors.gray1]}
          center
          middle
          white
          radius={2}>
          <TextInput
            keyboardType="decimal-pad"
            onChangeText={onChange}
            value={value}
          />
        </Block>
      </Block>
      <Block row margin={[5, 0, 0, 0]}>
        <Block gray1 height={2} flex={1} />
      </Block>
    </Block>
  );
};
