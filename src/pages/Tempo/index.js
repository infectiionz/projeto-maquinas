import React, {useState} from 'react';
import {TextInput, ScrollView} from 'react-native';
import {Block, Text, Colors} from '../../components';
import Card from './Card';
import Firebase from '../../services';

const Tempo = () => {
  const db = Firebase.database();

  const [inicioAcumuloInput, setInicioAcumuloInput] = useState('');

  const [atrasoAcumuloInput, setatrasoAcumuloInput] = useState('');

  const [atrasoStopMaquinaInput, setAtrasoStopMaquina] = useState('');
  const [tempoAvancoABAInput, setTempoAvancoABA] = useState('');
  const [tempoAtrasoABAInput, setTempoAtrasoABA] = useState('');
  const [tempoAvancoPistaoInput, setTempoAvancoPistao] = useState('');
  const [tempoAvancoMagazineInput, setTempoAvancoMagazine] = useState('');
  const [tempoVacuoArmacaoInput, setTempoVacuoArmacao] = useState('');

  return (
    <Block blackground flex={1}>
      <ScrollView>
        <Block margin={10} radius={7} color="#c0c0c0">
          <Block row margin={[10, 0, 0, 0]}>
            <Block flex={1} />
            <Block center middle size={[70, 20]}>
              <Text bold h2>
                Atual
              </Text>
            </Block>
            <Block center middle size={[70, 20]} margin={[0, 10, 0, 10]}>
              <Text bold h2>
                Set
              </Text>
            </Block>
          </Block>
          <Card
            value={inicioAcumuloInput}
            onChange={setInicioAcumuloInput}
            label="Inicio de acumulo"
            onPress={() =>
              db
                .ref('maquina1/tempo')
                .update({inicioAcumulo: inicioAcumuloInput})
            }
          />
          <Card
            value={atrasoAcumuloInput}
            onChange={setatrasoAcumuloInput}
            label="Atraso do acumulo"
            onPress={() =>
              db
                .ref('maquina1/tempo')
                .update({atrasoAcumulo: atrasoAcumuloInput})
            }
          />

          <Card
            value={atrasoStopMaquinaInput}
            onChange={setAtrasoStopMaquina}
            label="Atraso stop maquina"
            onPress={() =>
              db
                .ref('maquina1/tempo')
                .update({atrasoStopMaquina: atrasoStopMaquinaInput})
            }
          />
          <Card
            value={tempoAvancoABAInput}
            onChange={setTempoAvancoABA}
            label="Tempo avanço ABA"
            onPress={() =>
              db
                .ref('maquina1/tempo')
                .update({tempoAvancoABA: tempoAvancoABAInput})
            }
          />
          <Card
            value={tempoAtrasoABAInput}
            onChange={setTempoAtrasoABA}
            label="Tempo atraso ABA"
            onPress={() =>
              db
                .ref('maquina1/tempo')
                .update({tempoAtrasoABA: tempoAtrasoABAInput})
            }
          />
          <Card
            value={tempoAvancoPistaoInput}
            onChange={setTempoAvancoPistao}
            label="Tempo avanço pistão"
            onPress={() =>
              db
                .ref('maquina1/tempo')
                .update({tempoAvancoPistao: tempoAvancoPistaoInput})
            }
          />
          <Card
            value={tempoAvancoMagazineInput}
            onChange={setTempoAvancoMagazine}
            label="Tempo avanço magazine"
            onPress={() =>
              db
                .ref('maquina1/tempo')
                .update({tempoAvancoMagazineInput: tempoAvancoMagazineInput})
            }
          />
          <Card
            value={tempoVacuoArmacaoInput}
            onChange={setTempoVacuoArmacao}
            label="Tempo vacuo armação"
            onPress={() =>
              db
                .ref('maquina1/tempo')
                .update({tempoVacuoArmacao: tempoVacuoArmacaoInput})
            }
          />
        </Block>
      </ScrollView>
    </Block>
  );
};

export default Tempo;
