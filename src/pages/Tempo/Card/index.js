import React from 'react';
import {TextInput} from 'react-native';
import {Block, Text} from '../../../components';

// import { Container } from './styles';

const Card = ({onPress, label, onChange, value}) => {
  return (
    <Block>
      <Block margin={10} row height={50}>
        <Block flex={1} middle>
          <Block margin={[0, 0, 0, 10]}>
            <Text h2>{label}</Text>
          </Block>
        </Block>
        <Block
          center
          middle
          color="#f8f8f8"
          size={[70, 50]}
          border={[3, '#636571']}
          margin={[0, 10]}>
          <Text h2>30</Text>
        </Block>
        <Block>
          <Block color="#f8f8f8" size={[70, 50]} border={[3, '#636571']}>
            <TextInput
              style={{fontSize: 20}}
              value={value}
              onChangeText={onChange}
              keyboardType="decimal-pad"
              returnKeyType="done"
              onSubmitEditing={() => onPress()}
            />
          </Block>
        </Block>
      </Block>
    </Block>
  );
};

export default Card;
